const express = require('express');
const app = express();
const fs = require('fs');

const port = 8000;

app.use(express.json());


app.post('/message', (req, res) => {
  const datetime = Date.now();
  const path = `./messages/${datetime}.txt`;
  const content = JSON.stringify({
    message: req.body.message,
    datetime
  });
  fs.writeFile(path, content, err => {
    if (err) throw err;
    console.log('File saved');
    res.send(`File ${datetime} saved`);
  });
});



app.get('/message', (req, res) => {
  const path = './messages';
  const filePaths = [];
  fs.readdir(path, (err, files) => {
    files.forEach(file => {
      const filePath = `${path}/${file}`;
      filePaths.push(filePath);
    });
    console.log(filePaths, 'FILE PATHS')
    const content = [];
    const lastFive = filePaths.slice(-5);
    lastFive.forEach(filePath => {
      const data = fs.readFileSync(filePath, "utf8");
      content.push(data)
    });
    console.log(content, 'CONTENT');
    res.send(JSON.stringify(content));
  });
});

app.listen(port, () => {
  console.log('Server started');
});